package TestTransAccel;

class Switch  
 {
// Atrybuty:
	public DecisionNode base_DecisionNode;
	public String expr;
// Getters & Setters:
	public DecisionNode	getbase_DecisionNode() { return base_DecisionNode; }
	public void setbase_DecisionNode(DecisionNode w) { base_DecisionNode = w; }
	public String	getexpr() { return expr; }
	public void setexpr(String w) { expr = w; }
// Konstruktor bez parametrow:
	public Switch() {
		setbase_DecisionNode(new DecisionNode());
		setexpr("");
	}
// Konstruktor ze wszystkimi parametrami:
	public Switch(
	DecisionNode base_DecisionNodeParameter,
	String exprParameter
	) {
		setbase_DecisionNode(base_DecisionNodeParameter);
		setexpr(exprParameter);
	}
// Operacje:	

	
};
